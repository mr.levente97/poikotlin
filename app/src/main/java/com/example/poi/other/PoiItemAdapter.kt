package com.example.poi.other

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.poi.R
import com.example.poi.data.database.entities.POIItem
import com.example.poi.ui.poi.PoiViewModel
import kotlinx.android.synthetic.main.poi_item.view.*

class PoiItemAdapter
    (var items : List<POIItem>,
        private val viewModel : PoiViewModel)
    : RecyclerView.Adapter<PoiItemAdapter.PoiViewHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PoiViewHolder {

       val view = LayoutInflater.from(parent.context).inflate(R.layout.poi_item, parent, false)
        return PoiViewHolder(view)

    }

    override fun getItemCount(): Int {

        return items.size

    }

    override fun onBindViewHolder(holder: PoiViewHolder, position: Int) {

        val currentPoiItem = items[position]

        holder.itemView.tvName.text = currentPoiItem.name
        holder.itemView.tvLat.text = "${currentPoiItem.latitude}"
        holder.itemView.tvLong.text = "${currentPoiItem.longitude}"

        holder.itemView.ivDelete.setOnClickListener { viewModel.delete(currentPoiItem) }

    }

    inner class PoiViewHolder (itemView : View) : RecyclerView.ViewHolder(itemView)


}