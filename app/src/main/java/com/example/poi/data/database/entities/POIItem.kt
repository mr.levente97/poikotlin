package com.example.poi.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "poi_items")
data class POIItem (
    @ColumnInfo(name = "name")
    var name : String,

    @ColumnInfo(name = "longitude")
    var longitude : Double,

    @ColumnInfo(name = "latitude")
    var latitude : Double
)
{
    // ids will be auto generated
    @PrimaryKey(autoGenerate = true)
    var id : Int? = null;
}