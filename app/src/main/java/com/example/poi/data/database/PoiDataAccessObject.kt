package com.example.poi.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.poi.data.database.entities.POIItem

// SQL doesn't allow writing to DB in the main thread, so these should be called Async --> coroutines => suspend keyword
@Dao
interface PoiDataAccessObject {
    // if the row already exists just "update" it
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert_or_update(item: POIItem)

    @Delete
    suspend fun delete(item: POIItem)

    @Query("SELECT * FROM poi_items")
    fun get_all() : LiveData<List<POIItem>>
}