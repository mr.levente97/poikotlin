package com.example.poi

import android.app.Application
import com.example.poi.data.database.POIDatabase
import com.example.poi.data.repositories.PoiRepository
import com.example.poi.ui.poi.PoiViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class PoiApplication : Application(), KodeinAware {

    // Use application context during binding time
    override val kodein: Kodein = Kodein.lazy {

        import(androidXModule(this@PoiApplication))
        bind() from singleton {POIDatabase(instance())}
        bind() from singleton { PoiRepository(instance()) }
        bind() from provider {PoiViewModelFactory(instance())}

    }
}