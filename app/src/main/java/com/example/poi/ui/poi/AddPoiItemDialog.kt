package com.example.poi.ui.poi

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialog
import com.example.poi.R
import com.example.poi.data.database.entities.POIItem
import kotlinx.android.synthetic.main.dialog_add_poi_item.*

class AddPoiItemDialog
    (context : Context, var addDialogListener: AddPoiListener) : AppCompatDialog(context)
{
    override fun onCreate(savedInstanceState : Bundle?)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_add_poi_item)

        tvAdd.setOnClickListener {
            val name = etName.text.toString();
            val long = etLong.text.toString();
            val lat = etLat.text.toString();

            if(name.isEmpty() || long.isEmpty() || lat.isEmpty())
            {
                Toast.makeText(context, "Please fill out all information", Toast.LENGTH_SHORT).show();
                return@setOnClickListener;
            }

            val item = POIItem(name, long.toDouble(), lat.toDouble());
            addDialogListener.onAddButtonClicked(item);
            dismiss();

        }

        tvCancel.setOnClickListener { cancel(); }

    }
}