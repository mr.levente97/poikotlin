package com.example.poi.ui.poi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.poi.R
import com.example.poi.data.database.entities.POIItem
import com.example.poi.other.PoiItemAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MainActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory : PoiViewModelFactory by instance<PoiViewModelFactory>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModel = ViewModelProviders.of(this, factory).get(PoiViewModel::class.java);

        // RecycleView
        val adapter = PoiItemAdapter(listOf(), viewModel);

        rvPoiItems.layoutManager = LinearLayoutManager(this);
        rvPoiItems.adapter = adapter;

        // Display the items from the database in the RecycleView
        viewModel.get_all().observe(this, Observer {
            adapter.items = it;
            adapter.notifyDataSetChanged();
        })

        fab.setOnClickListener {
            AddPoiItemDialog(this,
            object : AddPoiListener
            {
                override fun onAddButtonClicked(item: POIItem) {
                    viewModel.insert_or_update(item);
                }

            }).show();
        }
    }
}