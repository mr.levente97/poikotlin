package com.example.poi.ui.poi

import com.example.poi.data.database.entities.POIItem

interface AddPoiListener {

    fun onAddButtonClicked(item : POIItem)

}