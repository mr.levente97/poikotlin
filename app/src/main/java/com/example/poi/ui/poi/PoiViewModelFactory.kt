package com.example.poi.ui.poi

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.poi.data.repositories.PoiRepository

@Suppress("UNCHECKED_CAST")
class PoiViewModelFactory
    (
        private val repository: PoiRepository
    ) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PoiViewModel(repository) as T
    }
}