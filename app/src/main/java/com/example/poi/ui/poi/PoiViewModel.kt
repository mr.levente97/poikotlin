package com.example.poi.ui.poi

import androidx.lifecycle.ViewModel
import com.example.poi.data.database.entities.POIItem
import com.example.poi.data.repositories.PoiRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PoiViewModel
    (
        private val repository: PoiRepository
    ) : ViewModel()
{
    // Room provides safety so we can run it on the main context
    fun insert_or_update(item : POIItem) = GlobalScope.launch {
        repository.insert_or_update(item)
    }

    fun delete(item : POIItem) = GlobalScope.launch {
        repository.delete(item)
    }

    fun get_all() = repository.get_all()

}